# Node.js serial port to mysql

Jenoduche ukladanie dat so serioveho portu do mysql.

## Getting Started

Stiahni repstori alebo cez prikazovy riadok spusti 
```
git clone https://gitlab.com/Plasto13/node-serial-mysql.git
```

### Installing

Nasledne spusti 

```
npm install
```

### A co teraz ?

Spusti script

```
node listport.js
```

