'use strict';

var mysql = require('mysql');
var database = "arduino";
var serialport = require('serialport');
const readline = require('readline');
const parsers = serialport.parsers;

var connection = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "",
  database : database
});

connection.connect(function(err) {
  if (err) throw err;
  // console.log(connection);
});

// Use a `\r\n` as a line terminator
const parser = new parsers.Readline({
  delimiter: '\r\n'
});


const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
  prompt: 'select port: '
});

var idx = 0;
var ports = [];

console.log('COM port list:');
serialport.list(function (err, p) {
  p.forEach(function(p) {
    ports.push(p.comName);
    console.log(' [' + idx + '] ' + p.comName);
    idx++;
  });

  rl.prompt();
 
  rl.on('line', (line) => {
    if(line<idx) {
      console.log('Opening ' + ports[Number(line)]);
     
      var port = new serialport(ports[Number(line)], {
        baudRate: 115200,
        dataBits: 8,
        parity: 'none',
        stopBits: 1,
        flowControl: false
        });
      
      port.on('error', function(e) {
        console.error(e.message);
        process.exit(0);
      }); 

  	port.on('open', function(){ 
	  	port.on('data', function(data){   
        data = JSON.parser(data);
        console.log('Recive data :', data); 
			  Object.keys(data).forEach(function(key) {
				if (key !="checksum") {
					var query = connection.query('INSERT INTO '+key+' SET ?', data[key], function (error, results, fields) {
							if (error) throw error;
							 // Neat!
					});
				console.log(query.sql); 					
				}
			});
		});
	}); 
    } else {
      console.error('ERROR: Wrong port number');
      process.exit(0);
    }
  });
 
  rl.on('close', () => {
  	 connection.end();
  	console.log('Bye! Conection mysql closed');
  	process.exit(0);
});
 
});